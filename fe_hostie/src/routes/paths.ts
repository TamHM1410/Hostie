const ROOTS ={
    AUTH:"/auth",
    DASHBOARD:"/dashboard"
    

}

export const paths={
    auth :{
        login:`${ROOTS.AUTH}/login`,
        register:`${ROOTS.AUTH}/register`
    },
    dashboard:{
        root:ROOTS.DASHBOARD,
        forum:`${ROOTS.DASHBOARD}/forum`,
        service:`${ROOTS.DASHBOARD}/service`,
        booking:`${ROOTS.DASHBOARD}/booking`,
        setting:`${ROOTS.DASHBOARD}/setting`,
        about:`${ROOTS.DASHBOARD}/about`
    }
}