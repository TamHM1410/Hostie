import dynamic from "next/dynamic";

const LandingView = dynamic(() => import("@/sections/landing/landing-view"), {
  ssr: false,
});

export default function Home() {
  return (
    <>
      <LandingView />
    </>
  );
}
