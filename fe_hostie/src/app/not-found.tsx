import NotfoundView from "@/sections/not-found/notfound-view";
export const metadata = {
  title: "404 Page Not Found!",
};
export default function NotFound() {
  return <NotfoundView />;
}
