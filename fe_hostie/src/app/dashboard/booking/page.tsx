import BookingView from "@/sections/dashboard/booking/bookingView";

export const metadata = {
  title: "Danh sách đặt phòng",
};

const Booking = () => {
  return <BookingView />;
};
export default Booking;
