"use client";
import DashboardLayout from "@/layout/DashboardLayout";
import AuthProvider from "../auth/AuthProvider";

type Props = {
  children: React.ReactNode;
};
export default function Layout({ children }: Props) {
  return (
    <AuthProvider>
      <DashboardLayout>{children}</DashboardLayout>
    </AuthProvider>
  );
}
