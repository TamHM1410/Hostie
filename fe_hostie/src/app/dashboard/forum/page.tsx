import ButtonMain from "@/components/ButtonMain";
import ForumView from "@/sections/dashboard/forum/ForumView";

export const metadata = {
  title: "Cộng đồng ",
};

const Forums = () => {
  return (
    <>
      <ForumView />
    </>
  );
};
export default Forums;
