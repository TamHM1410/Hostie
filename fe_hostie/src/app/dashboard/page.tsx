import DashboardView from "@/sections/dashboard/dashboard/dashboardView";
export const metadata = {
  title: "Dashboard ",
};
const Dashboard = () => {
  return (
    <>
      <DashboardView />
    </>
  );
};
export default Dashboard;
