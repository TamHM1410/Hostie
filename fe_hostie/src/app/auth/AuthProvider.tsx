"use client";
import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const router = useRouter();
  const [isLogin, setIslogin] = useState(true);

  useEffect(() => {
    if (isLogin == false) {
      router.push("/auth/login");
    }
  }, [isLogin]);

  return <>{children}</>;
};
export default AuthProvider;
