"use client";

///@hooks
import React, { useState } from "react";
import { useRouter } from "next/navigation";
///@antd
import type { FormProps } from "antd";
import { Button, Checkbox, Form, Input } from "antd";
///@next
import Image from "next/image";

//@image
import logo from "../../../public/images/logo.png";

type FieldType = {
  username?: string;
  email?: string;
  password?: string;
  remember?: string;
};

const onFinish: FormProps<FieldType>["onFinish"] = (values) => {
  console.log("Success:", values);
};

const onFinishFailed: FormProps<FieldType>["onFinishFailed"] = (errorInfo) => {
  console.log("Failed:", errorInfo);
};
const LoginForm = () => {
  const { push } = useRouter();

  const [formData, setFormData] = useState<FieldType>({
    email: "",
    password: "",
  });

  const handleChangeEmail = (e: any) => {
    setFormData({
      ...formData,
      password: e,
    });
  };
  console.log(formData);
  return (
    <Form
      name='basic'
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete='off'
    >
      <h1
        className='w-[315px] font-extrabold hover:cursor-pointer'
        style={{ fontSize: "80px", fontWeight: 800 }}
        onClick={() => push("/")}
      >
        HOST<span style={{ color: "#2152FF" }}>I</span>E
      </h1>
      <p className='font-bold	text-2xl'>Đăng nhập</p>
      <p style={{ fontSize: 12, color: "#67748E" }}>
        Nhập email và mật khẩu để tiếp tục
      </p>
      <Form.Item<FieldType>
        name='email'
        rules={[{ required: true, message: "Please input your email!" }]}
      >
        <Input
          placeholder='Email'
          className='md:w-[150%] max-sm:w-[100%] sm:w-[150%]'
          size='large'
          value={formData.email}
          onChange={(e) => handleChangeEmail(e.target.value)}
        />
      </Form.Item>

      <Form.Item<FieldType>
        name='password'
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input.Password
          placeholder='Password'
          className='md:w-[150%] max-sm:w-[100%] sm:w-[150%]'
          size='large'
        />
      </Form.Item>

      <Form.Item>
        <Button
          className='md:w-[150%] max-sm:w-[100%] sm:w-[150%]'
          type='primary'
          htmlType='submit'
          style={{
            background: "linear-gradient(to right, #2152FF, #21D4FD)",
            border: 0,
          }}
        >
          Đăng nhập
        </Button>
      </Form.Item>
      <Form.Item<FieldType>
        name='remember'
        valuePropName='checked'
        className='flex justify-center'
      >
        <p style={{ color: "#17C1E8", width: 200, textAlign: "center" }}>
          Quên mật khẩu
        </p>
      </Form.Item>
    </Form>
  );
};
export default LoginForm;
