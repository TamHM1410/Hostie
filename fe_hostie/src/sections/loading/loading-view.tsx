"use client";
import React from "react";
import { Alert, Flex, Spin } from "antd";

const contentStyle: React.CSSProperties = {
  padding: 100,
  //   background: "rgba(0, 0, 0, 0.05)",
  backgroundImage:
    "url(https://images.unsplash.com/photo-1562737794-88835094442e?q=80&w=2130&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)",
  borderRadius: 4,
};

const content = <div style={contentStyle} />;

export default function LoadingView() {
  return (
    <>
      <Flex gap='small' vertical className='min-h-screen'>
        <Flex
          gap='small'
          className=' flex items-center justify-center min-h-screen'
        >
          <Spin tip='Loading' size='large'></Spin>
        </Flex>
      </Flex>
    </>
  );
}
