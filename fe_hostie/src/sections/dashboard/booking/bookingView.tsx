import BookingHeader from "./bookingHeader";
import BookingContent from "./bookingContent";
import { BookingContentProvider } from "@/context/BookingContext/Booking";
export default function BookingView() {
  return (
    <>
      <BookingContentProvider>
        <BookingHeader />

        <BookingContent />
      </BookingContentProvider>
    </>
  );
}
