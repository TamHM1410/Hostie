"use client";

//@antd
import { Row, Col, Button, Input, Radio, Table } from "antd";
import { Icon } from "@iconify/react/dist/iconify.js";
import type { TableProps } from "antd";
///hook
import { useWindowSize } from "react-use";

import data from "./data.json";

interface DataType {
  id: number;
  username: string;
  booking_date: string;
  deposit: string;
  payment_method: string;
  status: number;
  action: any | undefined | null;
}

export default function BookingContent() {
  const handleAction = (id: number) => {
    console.log(`Action for ID: ${id}`);
  };

  const columns: TableProps<DataType>["columns"] = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Booking Date",
      dataIndex: "booking_date",
      key: "booking_date",
    },
    {
      title: "Deposit",
      dataIndex: "deposit",
      key: "deposit",
    },
    {
      title: "Payment Method",
      dataIndex: "payment_method",
      key: "payment_method",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action", // Add a unique key for the "Action" column
      render: (text: any, record: DataType) => (
        <button onClick={() => handleAction(record.id)}>{text}</button>
      ),
    },
  ];

  const rowClassName = (record: any, index: number) => {
    return index % 2 === 0 ? "row-even" : "row-odd";
  };

  return (
    <>
      <Row className='py-5'>
        <Col span={24} className='gap-2'>
          <Row>
            <Col md={3} sm={5}>
              <Button className='gap-2 flex items-center' size='large'>
                <span>
                  <Icon icon='fluent:triangle-down-12-filled' />
                </span>{" "}
                ID giao dịch
              </Button>
            </Col>
            <Col md={6} sm={19} className='max-sm:mt-2'>
              <Input
                size='large'
                placeholder='search'
                prefix={<Icon icon='clarity:search-line' />}
              />
            </Col>
            <Col
              sm={24}
              md={10}
              className='flex justify-center max-sm:mt-2 max-md:mt-2  ml-2'
            >
              <Radio.Group defaultValue='a' size='large'>
                <Radio.Button value='a'>Today</Radio.Button>
                <Radio.Button value='b'>Yesterday</Radio.Button>
                <Radio.Button value='c'>Last 7 days</Radio.Button>
                <Radio.Button value='d'>Last 30 days</Radio.Button>
                <Radio.Button value='e'>Last month</Radio.Button>
                <Radio.Button value='f'>2022-02-08 </Radio.Button>
              </Radio.Group>
            </Col>
            <Col span={4} className='flex items-center max-sm:mt-3 max-md:mt-2'>
              <Icon icon='streamline:sort-descending' width={35} />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row className='pt-10 '>
        <Col span={24} className='overflow-y-scroll	'>
          <Table
            columns={columns}
            dataSource={data}
            rowClassName={rowClassName}
          />
        </Col>
      </Row>
    </>
  );
}
