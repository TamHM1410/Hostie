"use client";

import { Input, Col, Row } from "antd";
import { Icon } from "@iconify/react/dist/iconify.js";
import { useWindowSize } from "react-use";

export default function Header() {
  const { width } = useWindowSize();
  return (
    <>
      <Row className='pb-5'>
        <Col md={16} sm={12}>
          <Col span={16}>
            <Input
              width='80%'
              size='large'
              placeholder='search'
              prefix={<Icon icon='clarity:search-line' />}
            />
          </Col>
        </Col>
        {width > 446 && (
          <Col md={8} sm={12} className='flex flex-end'>
            <h1
              className=' font-extrabold justify-center text-center hover:cursor-pointer max-sm:text-2xl	'
              style={{ fontSize: "30px", fontWeight: 800 }}
            >
              HOST<span style={{ color: "#2152FF" }}>I</span>E
            </h1>
          </Col>
        )}
      </Row>
    </>
  );
}
