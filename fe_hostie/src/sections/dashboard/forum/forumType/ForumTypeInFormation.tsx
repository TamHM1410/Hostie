
import TypeInFormation from "./TypeInFormation";
import TypeImage from "./TypeImage";

const ForumTypeInFormation = () => {
    return (
        <div className="grid place-items-center lg:flex lg:justify-between w-[1400px] max-w-full  ">

            <TypeInFormation />

            <TypeImage />

        </div>
    )
}

export default ForumTypeInFormation
