import Navbar from "@/components/Navbar"
import Services from "./Sevices"

const ServicesView = () => {
    return (
        <>
            <Navbar title={"Quản lí dịch vụ"} description={"Manage services"} />
            <div className="w-full border mt-5 rounded-lg flex justify-center ">

                <Services />


            </div>


        </>
    )
}
export default ServicesView