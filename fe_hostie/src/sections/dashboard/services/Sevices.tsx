"use client";
import { Button, Card, Pagination } from "antd";
import h1 from '../../../../public/images/2018-04-30-214088d759242733859024dd8690041f.webp'
import Image from "next/image";

const { Meta } = Card;

const Services = () => {
  return (
    <div className=" p-4  max-w-full w-[1400px] ">
      <div className="flex justify-end">
        <Button
          className="text-sm px-10 text-white"
          style={{ background: "linear-gradient(to right, #2152FF, #21D4FD)" }}
        >
          + Thêm dịch vụ
        </Button>
      </div>
      <div className="grid place-items-center gap-3 w-full mt-3 md:grid-cols-2 lg:grid-cols-3">
        {[...Array(6)].map((_, index) => (
          <Card
            key={index}
            hoverable
            className="w-full"
            cover={<Image src={h1} alt="Service Image" loading="lazy" />}
          >
            <Meta title="Europe Street beat" description="www.instagram.com" />
          </Card>
        ))}
      </div>
      <div className="flex justify-center w-full p-10">
        <Pagination defaultCurrent={1} total={50} />
      </div>
    </div>
  );
};

export default Services;
