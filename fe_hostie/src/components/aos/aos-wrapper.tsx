"use client";
import { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

const AOSWrapper = ({ children }: { children: React.ReactNode }) => {
  useEffect(() => {
    AOS.init({
      duration: 1200,
      offset: 100,
      delay: 0,
      once: false,
    });
  }, []);

  return <>{children}</>; // render nothing, as this component is just for initialization
};

export default AOSWrapper;
