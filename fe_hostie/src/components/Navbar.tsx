"use client"

import { BellOutlined, DownOutlined, UserOutlined } from "@ant-design/icons"
import { Avatar, Button, Dropdown, MenuProps, Space, message } from "antd"

const Navbar = ({ title, description }: { title: String, description: String }) => {
    const items: MenuProps['items'] = [
        {
            label: '1st menu item',
            key: '1',
            icon: <UserOutlined />,
        },
        {
            label: '2nd menu item',
            key: '2',
            icon: <UserOutlined />,
        },
        {
            label: '3rd menu item',
            key: '3',
            icon: <UserOutlined />,
            danger: true,
        },
        {
            label: '4rd menu item',
            key: '4',
            icon: <UserOutlined />,
            danger: true,
            disabled: true,
        },
    ];
    const handleMenuClick: MenuProps['onClick'] = (e) => {
        message.info('Click on menu item.');
        console.log('click', e);
    };
    const menuProps = {
        items,
        onClick: handleMenuClick,
    };
    return (
        <div className="flex justify-between ">
            <div>
                <h1 className="text-lg font-bold">{title}</h1>
                <p className="text-xs font-normal text-gray-400">{description}</p>
            </div>
            <div className="lg:flex md:flex gap-5 hidden" >
                <div className="bg-gray-100 rounded-lg cursor-pointer transition delay-150 duration-300 hover:bg-slate-400">
                    <BellOutlined className="p-3 transition delay-150 duration-300 hover:scale-125" />
                </div>
                <div>
                    <Dropdown menu={menuProps}>
                        <Button className="h-[42px] px-2">
                            <Space>
                                <Avatar shape="square" size={34} icon={<UserOutlined />} className="py-2" />
                                <span className="flex flex-col content-start">
                                    <h2 className="font-bold text-xs">TK Nguyen</h2>
                                    <p className="text-[10px]">Admin</p>
                                </span>
                                <DownOutlined style={{ fontSize: '10px', paddingLeft: '10px' }} />
                            </Space>
                        </Button>
                    </Dropdown>
                </div>
            </div>
        </div>
    )
}
export default Navbar