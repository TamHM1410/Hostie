"use client";

///hooks
import React from "react";
import Link from "next/link";
import { useState } from "react";
import { usePathname } from "next/navigation";
import { useRouter } from "next/navigation";

//@antd
import { Layout, Menu, theme } from "antd";
///asset
import {
  LayoutDashboard,
  Users,
  Settings,
  NotepadText,
  FolderKanban,
} from "lucide-react";
//paths
import { paths } from "@/routes/paths";

const { Header, Content, Footer, Sider } = Layout;

const items = [
  {
    key: 0,
    icon: <LayoutDashboard />,
    label: <Link href={paths.dashboard.root}>Dash Board</Link>,
  },
  {
    key: 1,
    icon: <Users />,
    label: <Link href={paths.dashboard.forum}>Cộng đồng</Link>,
  },
  {
    key: 2,
    icon: <FolderKanban />,
    label: <Link href={paths.dashboard.service}>Quản lí dịch vụ</Link>,
  },
  {
    key: 3,
    icon: <NotepadText />,
    label: <Link href={paths.dashboard.booking}>Danh sách đặt phòng</Link>,
  },
  {
    key: 4,
    icon: <Settings />,
    label: <Link href={paths.dashboard.setting}>Cài đặt</Link>,
  },
];

const DashboardLayout = ({ children }: any) => {
  const router = useRouter();

  const pathname = usePathname();

  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  let currPage;

  switch (pathname) {
    case "/dashboard":
      currPage = 0;
      break;
    case "/dashboard/forum":
      currPage = 1;
      break;
    case "/dashboard/booking":
      currPage = 2;
      break;
    case "/dashboard/setting":
      currPage = 4;
      break;
  }

  const [currentKey, setCurrentKey] = useState(0);

  return (
    <Layout className='min-h-screen'>
      <Sider
        style={{ backgroundColor: "#F6F5F2", minHeight: "100vh" }}
        breakpoint='lg'
        collapsedWidth='0'
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className='demo-logo-vertical' />
        <div>
          <h1
            className=' font-extrabold justify-center text-center hover:cursor-pointer'
            style={{ fontSize: "45px", fontWeight: 800 }}
            onClick={() => router.push("/")}
          >
            HOST<span style={{ color: "#2152FF" }}>I</span>E
          </h1>
        </div>
        <Menu
          style={{ backgroundColor: "#F6F5F2", border: 0 }}
          mode='inline'
          defaultSelectedKeys={[currPage?.toString() ?? "1"]}
          items={items}
          onClick={(e) => setCurrentKey(+e?.key)}
        />
      </Sider>
      <Layout>
        <Content style={{ margin: "24px 16px 0" }}>
          <div
            style={{
              padding: 24,
              minHeight: "100vh",
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Hostie Design ©{new Date().getFullYear()} Created by Hostie
        </Footer>
      </Layout>
    </Layout>
  );
};

export default DashboardLayout;
