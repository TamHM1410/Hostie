'use client'

import Navbar from "@/components/Navbar"
import ProfileSideBar from "@/sections/profile/ProfileSideBar"
import Link from "next/link"
import { usePathname, useRouter } from "next/navigation"

const ProfileLayout = ({ children }: any) => {
    const router = useRouter();
    const currentPath = usePathname();
    return (
        <>
            <Navbar title={"Trang cá nhân"} description={'Profile'} />
            <div className="md:flex md:flex-row mt-10 gap-10">

                <ProfileSideBar />

                <div className="md:basis-7/12 xl:basis-8/12 min-h-96 py-5 px-5 gap-5 border shadow-2xl rounded-lg mb-5 md:mb-0">
                    <div className="grid grid-cols-3  text-xs xl:text-sm w-full h-10 ">
                        <div className={currentPath === '/profile' ? 'border-b-2 border-blue-500 text-black' : 'border-b-2 text-gray-400 transition-colors duration-200 ease-in'}>
                            <Link href="/profile" className="text-center block">Thông tin cá nhân</Link>
                        </div>
                        <div className={currentPath === '/profile/changePassword' ? 'border-b-2 border-blue-500  text-black' : 'border-b-2 text-gray-400 transition-colors duration-200 ease-in '}>
                            <Link href="/profile/changePassword" className="text-center block">Đổi mật khẩu</Link>
                        </div>
                        <div className={currentPath === '/profile/notification' ? 'border-b-2 border-blue-500  text-black' : 'border-b-2 text-gray-400 transition-colors duration-200 ease-in'}>
                            <Link href="/profile/notification" className="text-center block">Thông báo</Link>
                        </div>
                    </div>
                    <div className="mt-10">{children}</div>
                </div>

            </div>
        </>
    )
}
export default ProfileLayout