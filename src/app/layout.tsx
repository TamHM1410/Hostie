import type { Metadata } from "next";
import { Inter, Roboto } from "next/font/google";
import AOSWrapper from "@/components/aos/aos-wrapper";
import "./globals.css";
const inter = Inter({ subsets: ["latin"] });
const roboto = Roboto({ weight: "500", style: "normal", subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Hostie",
  description: "Dịch vụ quản lí căn hộ /phòng cho thuê",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <AOSWrapper>
      <html lang='en'>
        <body className={roboto.className}>{children}</body>
      </html>
    </AOSWrapper>
  );
}
