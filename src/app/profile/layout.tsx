import DashboardLayout from "@/layout/DashboardLayout"
import ProfileLayout from "@/layout/ProfileLayout"
import React from "react"

const ProfileLayouts = ({ children }: { children: React.ReactNode }) => {
    return (
        <DashboardLayout>
            <ProfileLayout>{children}</ProfileLayout>
        </DashboardLayout>

    )
}
export default ProfileLayouts