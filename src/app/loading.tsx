import LoadingView from "@/sections/loading/loading-view";
export default function Loading() {
  return <LoadingView />;
}
