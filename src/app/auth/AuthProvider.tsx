import { redirect } from "next/navigation";
import { useRouter } from "next/navigation";
const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const router = useRouter();
  const isLogin = false;
  if (!isLogin) {
    redirect("/auth/login");
  }

  return <>{children}</>;
};
export default AuthProvider;
