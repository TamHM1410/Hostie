
export type BookingType={
    id: number;
    username: string;
    booking_date: string;
    deposit: string;
    payment_method: string;
    status: number;

}

