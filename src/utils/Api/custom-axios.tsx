import axios from "axios";

const url = process.env.NEXT_PUBLIC_API_URL;

const instance = axios.create({
  baseURL: url,
  timeout: 1000,
  headers: { "X-Custom-Header": "foobar" },
});

instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    const token = window.localStorage.getItem("token"); //do not store token on localstorage!!!
    config.headers.Authorization = token;
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default instance;
