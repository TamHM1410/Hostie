"use client";
import {
  createContext,
  useContext,
  useState,
  Dispatch,
  SetStateAction,
} from "react";
import { BookingType } from "@/type/booking";

const defaultValue: BookingType = {
  id: 0,
  username: "",
  deposit: "300k",
  booking_date: "293",
  payment_method: "Vnpay",
  status: 0,
};

type BookingContextType = {
  currentBooking: BookingType;
  setCurrentBooking: Dispatch<SetStateAction<BookingType>>;
};

export const BookingContext = createContext<BookingContextType>({
  currentBooking: defaultValue,
  setCurrentBooking: () => {},
});

export function BookingContentProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [currentBooking, setCurrentBooking] =
    useState<BookingType>(defaultValue);

  return (
    <BookingContext.Provider value={{ currentBooking, setCurrentBooking }}>
      {children}
    </BookingContext.Provider>
  );
}
