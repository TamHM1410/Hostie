"use client";

///@hook
import { useWindowSize } from "react-use";
import dynamic from "next/dynamic";

///@component
const LoginForm = dynamic(() => import("./loginForm"), { ssr: false });
// import LoginForm from "./loginForm";
import Image from "next/image";
import LoginProvider from "@/app/auth/LoginProvider";
///@imgae
import bg from "../../assets/auths/auth.jpeg";

const LoginView = (props: any) => {
  const { width } = useWindowSize();

  return (
    <>
      <LoginProvider>
        {" "}
        {width < 1024 ? (
          <div className='flex flex-col w-screen min-h-screen items-center bg-blue-100'>
            <div className='w-full min-h-screen flex justify-center items-center'>
              <LoginForm />
            </div>
          </div>
        ) : (
          <div className='flex flex-row w-screen min-h-screen '>
            <div
              className='w-1/2 flex justify-center '
              style={{ alignItems: "center" }}
            >
              <LoginForm />
            </div>
            <div className='w-1/2 min-h-screen   flex items-center  mr-5 '>
              <Image
                style={{ borderRadius: 20, marginLeft: 5 }}
                className='hover:cursor-pointer hover:w-[1000px] shadow-3xl	'
                width={900}
                height={900}
                alt=''
                src={bg}
              />
            </div>
          </div>
        )}
      </LoginProvider>
    </>
  );
};
export default LoginView;
