"use client"
import {
    Button,
    Form,
    Input,
    InputNumber,
    Select,
} from 'antd';



const ProfileView = () => {
    return (
        <div className=' grid place-items-center p-4'>
            <Form
                labelCol={{
                    xs: { span: 8 }, lg: { span: 4 }, md: {
                        span: 8
                    }
                }}
                wrapperCol={{ span: 16 }}
                style={{ maxWidth: 1200 }}

                className='w-full '
            >
                <Form.Item
                    label="Họ và Tên"
                    name="name"
                    rules={[{ required: true, message: 'Please input your name!' }]}


                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Tuổi"
                    name="age"
                    rules={[{ required: true, message: 'Please input your age!' }]}

                >
                    <InputNumber style={{ width: '100%' }} />
                </Form.Item>
                <Form.Item

                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item

                    label="Tài khoản"
                    name="account"
                    rules={[{ required: true, message: 'Please input your account!' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Giới tính"

                    name="gender"
                    rules={[{ required: true, message: 'Please select your gender!' }]}
                >
                    <Select>
                        <Select.Option value="male">Nam</Select.Option>
                        <Select.Option value="female">Nữ</Select.Option>
                        <Select.Option value="other">Khác</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="BIO"
                    name="bio"
                    rules={[{ required: true, message: 'Please input your bio!' }]}
                >
                    <Input.TextArea />
                </Form.Item>
                <div className='flex w-full' >
                    <Button type="primary" htmlType="submit" className='mx-auto w-2/3 '>
                        Lưu và cập nhật
                    </Button>
                </div>
            </Form>
        </div>
    );
};

export default ProfileView;
