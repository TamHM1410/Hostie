import { AntDesignOutlined } from "@ant-design/icons"
import { Avatar } from "antd"

const ProfileSideBar = () => {
    return (
        <div className="md:basis-5/12 xl:basis-3/12 min-h-96 grid place-items-center py-5 px-5 xl:px-5  md:px-5 gap-5 border shadow-2xl rounded-lg mb-5 md:mb-0">
            <Avatar
                size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 200 }}
                icon={<AntDesignOutlined />}
            />
            <p className="text-xl font-bold">TK Nguyen</p>
            <p className="text-base text-gray-400 font-normal">Chủ nhà</p>
            <p className=" w-full  flex justify-between p-4  border-solid border rounded-lg shadow-xl"><span>Lượt đánh giá</span> <span className="text-gray-400 text-xs"> <strong className="text-blue-500 text-xs md:text-sm">150</strong> đánh giá</span></p>
            <p className="w-full flex justify-between p-4    border-solid border  rounded-lg shadow-xl"><span>Số lượng nhà</span> <span className="text-gray-400 text-xs"> <strong className="text-blue-500 text-xs md:text-sm">6</strong> căn/phòng</span></p>
            <p className="w-full  flex justify-between p-4   border-solid border rounded-lg shadow-xl"><span>Người dùng từ</span> <strong className="text-blue-500 text-xs md:text-sm">20/6/2021</strong></p>
            <p className="text-gray-400  mt-4 text-xs text-center">Chủ nhà hứa hẹn sẽ cung cấp cho bạn một không gian sống thoải mái và an toàn trong thời gian lưu trú.
                Rất mong được đón tiếp bạn tại căn hộ của TK Nguyen. Nếu có bất kỳ câu hỏi nào, xin vui lòng liên hệ 0917250012</p>
        </div>
    )
}
export default ProfileSideBar