"use client";

///@hooks

import { useRouter } from "next/navigation";
import { useWindowSize } from "react-use";

///@antd
import { Col, Row, Typography, Button } from "antd";

///@components
import AOSWrapper from "@/components/aos/aos-wrapper";

///@next
import Link from "next/link";
import Image from "next/image";

//@config
import { paths } from "@/routes/paths";

///@asset
import logo from "../../../public/images/logo.png";
import landingImage from "../../../public/images/landing-hero.jpg";

export default function LandingHero() {
  const { width } = useWindowSize();

  const router = useRouter();

  return (
    <>
      <AOSWrapper>
        <Row
          className='px-5 min-h-screen py-5 '
          style={{ backgroundColor: "#5884E7" }}
        >
          <Col span={24} className='flex items-center justify-center'>
            <Row className='py-10 flex items-center gap-10'>
              <Col md={11} sm={24} className='flex text-center  justify-center'>
                <Col className='' sm={24}>
                  <Typography className='max-sm:flex max-sm:justify-center'>
                    <Image
                      src={logo}
                      alt='hostie'
                      width={250}
                      height={250}
                      onClick={() => router.push(paths.dashboard.about)}
                    />
                  </Typography>
                  <Typography.Title
                    level={1}
                    className='py-5 max-w-[350px] text-left max-sm:text-center max-sm:max-w-screen '
                    style={{ color: "#c4e3ff" }}
                  >
                    Quản lý dịch vụ đặt phòng một cách đơn giản.
                  </Typography.Title>
                  <Typography.Paragraph className=' max-w-[350px] text-left max-sm:text-center'>
                    Xin cảm ơn quý khách đã lựa chọn dịch vụ của chúng tôi.Trong
                    trường hợp cần hỗ trợ thêm, vui lòng liên hệ số điện thoại
                    hotline của chúng tôi.
                  </Typography.Paragraph>
                  <Typography>
                    <Row className='gap-5'>
                      <Link href={paths.dashboard.root}> </Link>
                      <Button
                        size='large'
                        className=' font-bold	md:w-[100%] max-sm:w-[100%]   '
                        style={{ color: "#5884E7" }}
                        onClick={() => router.push(paths.dashboard.root)}
                      >
                        Getting Started
                      </Button>

                      <Button
                        size='large'
                        className='font-bold bg-black text-stone-50 border-0 md:w-[100%] max-sm:w-[100%]'
                      >
                        Purchase
                      </Button>
                    </Row>
                  </Typography>
                </Col>
              </Col>
              {width > 768 && (
                <Col md={11} className='flex justify-center'>
                  <Col className='py-5'>
                    <Image
                       className='rounded-lg transition duration-1000 ease-in-out hover:cursor-pointer hover:w-[530px] hover:shadow-3xl'
                      data-aos='zoom-in'
                      width={500}
                      height={400}
                      alt='marketing market'
                      src={landingImage}
                    />
                  </Col>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </AOSWrapper>
    </>
  );
}
