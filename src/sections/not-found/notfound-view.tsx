import page from "../../assets/404/223936-P1DFL6-644.jpg";
import Link from "next/link";
import { Button } from "antd";
export default function NotfoundView() {
  return (
    <div
      className='min-h-screen min-w-screen flex  justify-center items-center'
      style={{
        background: `url(${page.src}) no-repeat center`,
        backgroundSize: "cover",
      }}
    >
      <Link href='/' className='mt-2'>
        <Button size='large'>
          <h1>Trở về trang chủ</h1>
        </Button>
      </Link>
    </div>
  );
}
