import dynamic from "next/dynamic";

const Header = dynamic(() => import("./dashboardHeader"), { ssr: false });

const DashboardContent = dynamic(() => import("./dashboardContent"), {
  ssr: false,
});

const LoadingView = dynamic(() => import("@/sections/loading/loading-view"), {
  ssr: true,
});

export default function DashboardView() {
  // const login = true;
  // if (!login) {
  //   return <LoadingView />;
  // }
  return (
    <>
      <Header />

      <DashboardContent />
    </>
  );
}
