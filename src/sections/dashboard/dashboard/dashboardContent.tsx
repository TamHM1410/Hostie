"use client";

///@antd
import { Col, Row, Typography, Button } from "antd";

///asset
import { ArrowUp, ArrowDown } from "lucide-react";

///ch
import {
  optionsColumn,
  optionDonut,
  lineChart,
} from "../../../config/chartConfig";

///next
import dynamic from "next/dynamic";
///hooks
import { useWindowSize } from "react-use";

const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });

export default function DashboardContent() {
  const { width } = useWindowSize();
  const data = [30, 40, 30];
  return (
    <>
      <Row
        className='py-5  pt-18  overflow-x-scroll'
        style={{ borderTop: "1px solid", borderColor: "#C8CBD9       " }}
      >
        <Col span={24}>
          <Row>
            <Col md={16} sm={24}>
              <Typography.Title level={3} style={{ color: "#1F384C" }}>
                Dashboard
              </Typography.Title>
              <Typography style={{ fontSize: "24px" }}>
                Doanh Thu Tổng Các Dịch Vụ
              </Typography>
              <p style={{ fontSize: "24px", fontWeight: "bolder" }}>
                IDR 7.852.000
              </p>
              <Row>
                <ArrowUp
                  size={20}
                  style={{ color: "#149D52" }}
                  className='flex items-center'
                />

                <Typography.Text className=''>
                  <span style={{ color: "#149D52" }}>2.1%</span> vs last week
                </Typography.Text>
              </Row>
              <Typography>
                <Chart
                  options={optionsColumn}
                  series={optionsColumn.series}
                  type='bar'
                  height={350}
                />
              </Typography>
            </Col>
            <Col
              md={8}
              sm={24}
              style={{
                borderLeft: width < 768 ? 0 : "1px solid",
                borderColor: "#C8CBD9",
              }}
              className='px-5 '
            >
              <Typography.Title level={3} style={{ color: "#1F384C" }}>
                Dashboard
              </Typography.Title>
              <Typography style={{ fontSize: "24px" }}>
                Doanh Thu Tổng Các Dịch Vụ
              </Typography>
              <p style={{ fontSize: "24px", fontWeight: "bolder" }}>
                IDR 7.852.000
              </p>
              <Typography className='flex justify-center overflow-auto	'>
                <Chart
                  options={optionDonut}
                  series={optionDonut.series}
                  type='donut'
                  height={width > 700 ? 350 : 200}
                  width={350}
                />
              </Typography>
            </Col>
          </Row>
        </Col>
        <Col
          span={24}
          style={{ borderTop: "1px solid", borderColor: "#C8CBD9       " }}
        >
          <Row className='py-2  gap-0.2'>
            <Col md={8} className='flex justify-center' sm={24}>
              <Typography>
                <Typography.Title level={4} className=''>
                  Trạng thái các đơn đã booking
                </Typography.Title>
                <Typography className='pt-10'>
                  <Row className='flex justify-center'>
                    <Col
                      span={18}
                      className='flex gap-8 pb-3 justify-center'
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "yellow ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "yellow ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center max-w-[120px] min-w-[120px]'>
                        Đơn tạo mới
                      </Typography.Text>

                      <Typography.Text className='flex items-center min-w-[50px]'>
                        90
                      </Typography.Text>
                    </Col>
                    <Col
                      span={18}
                      className='flex gap-8 py-3 justify-center'
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "green",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "green",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center max-w-[120px] min-w-[120px]'>
                        Đơn Đã Cọc
                      </Typography.Text>

                      <Typography.Text className='flex items-center min-w-[50px]'>
                        13
                      </Typography.Text>
                    </Col>
                    <Col
                      span={18}
                      className='flex gap-8 py-3 justify-center  '
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "blue ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "blue",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center max-w-[120px] min-w-[120px]'>
                        Đơn Đã Checkin
                      </Typography.Text>

                      <Typography.Text className='flex items-center  min-w-[50px]'>
                        123
                      </Typography.Text>
                    </Col>
                    <Col
                      span={18}
                      className='flex gap-8 py-3 justify-center'
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "#EA0606  ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "#EA0606  ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center  max-w-[120px] min-w-[120px]'>
                        Đơn Đã hủy
                      </Typography.Text>

                      <Typography.Text className='flex items-center min-w-[50px]'>
                        123
                      </Typography.Text>
                    </Col>
                  </Row>
                </Typography>
              </Typography>
            </Col>
            <Col
              md={8}
              sm={24}
              className='px-5'
              style={{
                borderLeft: width < 768 ? 0 : "0.5px solid",
                borderColor: "#C8CBD9",
              }}
            >
              <Typography>
                <Typography.Title level={4} className=''>
                  Dịch vụ đã booking{" "}
                </Typography.Title>
                <Typography className='pt-10'>
                  <Row className='flex justify-center'>
                    <Col
                      span={18}
                      className='flex gap-8 pb-3 justify-center'
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "yellow ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "yellow ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center max-w-[120px] min-w-[120px]'>
                        Đơn tạo mới
                      </Typography.Text>

                      <Typography.Text className='flex items-center min-w-[50px]'>
                        90
                      </Typography.Text>
                    </Col>
                    <Col
                      span={18}
                      className='flex gap-8 py-3 justify-center'
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "green ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "green",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center max-w-[120px] min-w-[120px]'>
                        Đơn Đã Cọc
                      </Typography.Text>

                      <Typography.Text className='flex items-center min-w-[50px]'>
                        13
                      </Typography.Text>
                    </Col>
                    <Col
                      span={18}
                      className='flex gap-8 py-3 justify-center  '
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "blue ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "blue ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center max-w-[120px] min-w-[120px]'>
                        Đơn Đã Checkin
                      </Typography.Text>

                      <Typography.Text className='flex items-center  min-w-[50px]'>
                        123
                      </Typography.Text>
                    </Col>
                    <Col
                      span={18}
                      className='flex gap-8 py-3 justify-center'
                      style={{ borderBottom: "0.5px solid", color: "#DBE5EB" }}
                    >
                      {width > 1050 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "#EA0606  ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      {width < 768 && (
                        <span
                          className='min-w-[30px] max-h-[30px]   min-h-[30px] rounded shadow-md'
                          style={{
                            backgroundColor: "#EA0606  ",
                            borderRadius: 30,
                          }}
                        ></span>
                      )}
                      <Typography.Text className='flex items-center  max-w-[120px] min-w-[120px]'>
                        Đơn Đã hủy
                      </Typography.Text>

                      <Typography.Text className='flex items-center min-w-[50px]'>
                        123
                      </Typography.Text>
                    </Col>
                  </Row>
                </Typography>
              </Typography>
            </Col>
            <Col
              md={8}
              sm={24}
              className='px-5'
              style={{
                borderLeft: width < 768 ? 0 : "0.5px solid",
                borderColor: "#C8CBD9",
              }}
            >
              <Typography>
                <Row>
                  <Col span={16} className='flex items-center'>
                    <Typography.Title level={4} className=''>
                      Hoa hồng / số đơn
                    </Typography.Title>
                  </Col>
                  <Col span={8} className='flex items-center'>
                    <Button
                      style={{ color: "#5A6ACF", width: 100 }}
                      size='small'
                    >
                      Chi tiết
                    </Button>
                  </Col>
                </Row>
                <Typography>2.568</Typography>
                <Row>
                  <ArrowDown
                    size={20}
                    style={{ color: "#F2383A" }}
                    className='flex items-center'
                  />

                  <Typography.Text className=''>
                    <span style={{ color: "#F2383A" }}>2.1%</span> vs last week
                  </Typography.Text>
                </Row>
                <Typography.Text className='pt-2'>
                  Sales from 1-6 Dec, 2020
                </Typography.Text>
                <Row>
                  <Col span={24} className='flex justify-center'>
                    <Chart
                      options={lineChart}
                      series={lineChart.series}
                      type='line'
                      width='350'
                    />
                  </Col>
                </Row>
              </Typography>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
}
