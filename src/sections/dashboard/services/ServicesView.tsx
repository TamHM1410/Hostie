import Navbar from "@/components/Navbar"
import Services from "./Sevices"

const ServicesView = () => {
    return (
        <>
            <Navbar title={"Quản lí dịch vụ"} description={"Manage services"} />
            <Services />

        </>
    )
}
export default ServicesView