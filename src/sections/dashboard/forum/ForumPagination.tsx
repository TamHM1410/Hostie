import { Pagination, Row, Col } from "antd";

export default function ForumPagination() {
  return (
    <>
      <Row>
        <Col span={24} className='flex justify-center'>
          {" "}
          <Pagination defaultCurrent={1} total={50} />
        </Col>
      </Row>
    </>
  );
}
