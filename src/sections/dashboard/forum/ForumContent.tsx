"use client";
import { Row, Col, Typography, Button } from "antd";
import { Icon } from "@iconify/react/dist/iconify.js";
import { useWindowSize } from "react-use";

export default function ForumContent() {
  const { width } = useWindowSize();
  return (
    <>
      <Row
        className='min-h-screen py-10  px-10 my-5'
        style={{
          border: "1px solid",
          borderColor: "#A2A1A8 ",
          borderRadius: "10px",
        }}
      >
        <Col span={24}>
          <Col
            className='villa flex justify-center justify-evenly flex-wrap mt-5 gap-20'
            style={{ overflow: "hidden" }}
          >
            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>
            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>
            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>
            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>
            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>

            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>

            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>

            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>

            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>

            <Col
              span={15}
              className=' max-h-[171px] max-w-[301px] py-5 px-5 flex-row'
              style={{ backgroundColor: "#e5e7eb", borderRadius: 5 }}
            >
              <Typography.Title
                level={3}
                className='flex flex-row gap-3 items-center'
              >
                <span>
                  <Icon icon='uit:bag' width={25} />
                </span>
                Villa
              </Typography.Title>
              <Typography className='gap-5'>
                <Button style={{ backgroundColor: "#7152F3", color: "#fff" }}>
                  {" "}
                  check in :14h
                </Button>
                <Button
                  style={{
                    backgroundColor: "#7152F3",
                    color: "#fff",
                    marginLeft: width < 390 ? 0 : 8,
                  }}
                >
                  {" "}
                  check out:12h
                </Button>
              </Typography>
              <Typography
                className='flex flex-row gap-2 items-center '
                style={{ marginTop: width < 390 ? 4 : 8 }}
              >
                <span>
                  <Icon icon='ep:location' width={25} />{" "}
                </span>
                Số lượng :36
              </Typography>
            </Col>
          </Col>
        </Col>
      </Row>
    </>
  );
}
