import HeaderForum from "./Forumheader";
import ForumContent from "./ForumContent";
import ForumPagination from "./ForumPagination";

const ForumView = () => {
  return (
    <>
      <HeaderForum />

      <ForumContent />

      <ForumPagination />
    </>
  );
};
export default ForumView;
