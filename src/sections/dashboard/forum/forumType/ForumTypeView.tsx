
"use client"
import Navbar from "@/components/Navbar"
import ForumTypeFilter from "./ForumTypeFilter"
import { Button } from "antd"
import { useState } from "react"
import ForumTypeInFormation from "./ForumTypeInFormation"
import BookingDashboard from "./Booking"

const ForumTypeView = () => {
    const [view, setView] = useState(false)
    const showSearch = () => {
        setView(!view)
    }
    const year = new Date().getFullYear(); // Lấy năm hiện tại
    const currentMonth = new Date().getMonth() + 1; // Lấy tháng hiện tại (từ 0 đến 11)
    return (
        <>

            <Navbar title={"Cộng đồng/ Vũng Tàu/ Villa"} description={"Forum type"} />

            <div className="flex justify-center lg:justify-start lg:mb-5 border-t mt-5" >
                <Button className="border-0 font-medium text-sm px-10   text-white mt-5 " onClick={() => showSearch()} style={{
                    background: "linear-gradient(to right, #2152FF, #21D4FD)",
                    border: 0,
                }}>Tìm kiếm Nâng Cao</Button>
            </div>

            <ForumTypeFilter searchVisible={view} />

            <ForumTypeInFormation />

            <BookingDashboard year={year} month={currentMonth} />

        </>
    )
}
export default ForumTypeView