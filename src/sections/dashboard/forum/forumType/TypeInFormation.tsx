import { FireOutlined, LinkOutlined, PhoneOutlined } from "@ant-design/icons"
import { Divider } from "antd"


const TypeInFormation = () => {
    return (
        <div className="w-full">
            <p className="text-2xl">1 Trần Quý Cáp</p>
            <span className="flex gap-3 items-center mt-5">
                <a href="/" className="font-normal">0773828226 <PhoneOutlined className="text-blue-300" /></a> <Divider type="vertical" className="h-5 " />
                <a href="/" className="font-normal">Google Drive <LinkOutlined className="text-blue-300" /></a> <Divider type="vertical" className="h-5 " />
                <a href="/" className="font-normal">Zalo <LinkOutlined className="text-blue-300" /></a>
            </span>
            <p className="mt-5 font-normal"><a href="/">1 Trần Bình Quý Cáp, Phường Tam Thắng, Thành Phố Vũng Tàu,Bà Rịa Vũng Tàu <LinkOutlined className="text-blue-300" /></a></p>
            <div className="mt-5 font-normal gap-3 flex lg:gap-9 flex-wrap">
                <p ><FireOutlined className="text-blue-300" /> Bida</p>
                <p><FireOutlined className="text-blue-300" /> Karaoke</p>
                <p><FireOutlined className="text-blue-300" /> Hồ bơi </p>
                <p><FireOutlined className="text-blue-300" /> Tiêu chuẩn : 15 người</p>
            </div>
        </div>
    )
}

export default TypeInFormation
