import { DownOutlined } from "@ant-design/icons"
import { Dropdown, Space } from "antd"

export const Filter = ({ items, onClick, type, title }: { items: any, onClick: any, title: String, type: String }) => {
    return (


        <div className="flex flex-col w-full">
            <span className="text-xs font-normal px-3 ">{type}</span>
            <Dropdown menu={{ items, onClick }} className="border p-3 rounded-md ">
                <a onClick={(e) => e.preventDefault()}>
                    <Space className="flex justify-between">
                        <span className="font-semibold text-sm"> {title}</span>
                        <DownOutlined />
                    </Space>
                </a>
            </Dropdown>
        </div>

    )
}