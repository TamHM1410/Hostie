
"use client"
import { Button, Dropdown, MenuProps, Space, message } from "antd"
import { Filter } from "./Filter";
import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;
const items: MenuProps['items'] = [
    {
        label: '1st menu item',
        key: '1',
    },
    {
        label: '2nd menu item',
        key: '2',
    },
    {
        label: '3rd menu item',
        key: '3',
    },
];
const ForumTypeFilter = ({ searchVisible }: { searchVisible: any }) => {
    const onClick: MenuProps['onClick'] = ({ key }) => {
        message.info(`Click on item ${key}`);
    };
    return (
        <div className={`flex flex-col gap-3 lg:flex-row justify-center mb-8 lg:mb-0 items-center lg:gap-10 mt-5 transition-all  duration-500 ease-in-out ${searchVisible ? '' : 'h-0'} ${searchVisible ? 'opacity-100' : 'opacity-0'}  ${searchVisible ? 'visible' : 'invisible'}`}  >
            <Filter items={items} onClick={onClick} key={1} type={"Thương Hiệu"} title={"Chọn thương hiệu "} />
            <Filter items={items} onClick={onClick} key={2} type={"Loại chỗ ở"} title={"Chọn loại chỗ ở "} />
            <div className="flex flex-col w-full">
                <span className="text-xs font-normal px-3 ">Chọn Ngày</span>
                <RangePicker className="w-full p-3" />
            </div>

            <Button className="border-0 font-medium text-sm px-10   text-white  " style={{

                background: "linear-gradient(to right, #2152FF, #21D4FD)",
                border: 0,
            }}>Tìm kiếm </Button>
        </div>
    )
}
export default ForumTypeFilter
