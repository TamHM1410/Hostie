import { Carousel } from "antd";
import Image from "next/image";

import h12 from '../../../../../public/images/Unveiling_the_Splendor.jpg.webp'
import h3 from '../../../../../public/images/thumb-b641ed1ff23f1d363a5827010814c6fe-cli096_pool_01.jpg'
import h4 from '../../../../../public/images/villa-khong-gian-sang-trong.webp'

const TypeImage = () => {
    return (
        <div className="w-full lg:place-content-end max-w-[500px] overflow-hidden rounded-lg">
            <Carousel autoplay className="max-w-[600px] mt-10 w-full" >
                <div className="w-auto">
                    <Image src={h12}
                        height={500} priority
                        alt="tt" />
                </div>
                <div className="w-auto">
                    <Image src={h12}
                        height={500}
                        alt="tt"
                        priority
                    />
                </div>
                <div className="w-auto">
                    <Image src={h3}
                        height={500}
                        alt="tt"
                        priority />
                </div>
                <div className="w-auto">
                    <Image src={h4}
                        height={500}
                        alt="tt"
                        priority />
                </div>
            </Carousel>
        </div>
    );
};

export default TypeImage;
