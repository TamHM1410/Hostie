
import TypeInFormation from "./TypeInFormation";
import TypeImage from "./TypeImage";

const ForumTypeInFormation = () => {
    return (
        <div className="grid place-items-center lg:flex lg:justify-between w-full  ">

            <TypeInFormation />

            <TypeImage />

        </div>
    )
}

export default ForumTypeInFormation
