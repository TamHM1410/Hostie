import React, { useState } from "react";
import { Table, Select } from "antd";

const { Option } = Select;

interface DayInfo {
  day: number;
  dayOfWeek: string;
}

function getDaysInMonth(year: number, month: number): DayInfo[] {
  const daysInMonth = new Date(year, month, 0).getDate();
  const days: DayInfo[] = [];

  for (let i = 1; i <= daysInMonth; i++) {
    const date = new Date(year, month - 1, i);
    const options: any = { weekday: "short" };
    const dayOfWeek = new Intl.DateTimeFormat("us-EN", options).format(date);
    days.push({ day: i, dayOfWeek });
  }

  return days;
}

interface BookingDashboardProps {
  year: number;
  month: number;
}

const BookingDashboard: React.FC<BookingDashboardProps> = ({ year, month }) => {
  const [selectedMonth, setSelectedMonth] = useState(month);
  const daysInMonth = getDaysInMonth(year, selectedMonth);

  const handleMonthChange = (value: any) => {
    setSelectedMonth(value);
  };

  const columns = [
    {
      title: (
        <Select
          defaultValue={selectedMonth}
          style={{ width: 240 }}
          className='w-96 h-10'
          onChange={handleMonthChange}
        >
          <Option value={1}>Tháng 1</Option>
          <Option value={2}>Tháng 2</Option>
          <Option value={3}>Tháng 3</Option>
          <Option value={4}>Tháng 4</Option>
          <Option value={5}>Tháng 5</Option>
          <Option value={6}>Tháng 6</Option>
          <Option value={7}>Tháng 7</Option>
          <Option value={8}>Tháng 8</Option>
          <Option value={9}>Tháng 9</Option>
          <Option value={10}>Tháng 10</Option>
          <Option value={11}>Tháng 11</Option>
          <Option value={12}>Tháng 12</Option>
        </Select>
      ),
      dataIndex: "monthSelect",
      key: "monthSelect",
    },
  ];

  const columnss = [
    ...daysInMonth.map((day) => ({
      title: (
        <p
          className={`w-24 h-10 text-center ${
            ["Sat", "Sun", "Fri"].includes(day.dayOfWeek) ? "text-red-500" : ""
          }`}
        >
          {day.dayOfWeek + " - " + day.day}
        </p>
      ),
      dataIndex: day.day.toString(),
      key: day.day,
    })),
  ];

  const dataSource = [
    {
      key: "monthSelect",
      monthSelect: <p> 1 Trần Quý Cáp</p>,
    },
  ];

  const dataSources = [
    {
      key: "days",
      ...daysInMonth.reduce((obj, day) => ({ ...obj, [day.day]: day.day }), {}),
    },
  ];
  return (
    <div className=' mt-10'>
      <h2 className='mb-5 text-2xl'>Đặt phòng dựa trên lịch </h2>
      <div className='flex'>
        <Table
          columns={columns}
          dataSource={dataSource}
          bordered
          pagination={false}
        />
        <Table
          columns={columnss}
          dataSource={dataSources}
          bordered
          className='  overflow-auto'
          pagination={false}
        />
      </div>
    </div>
  );
};

export default BookingDashboard;
