"use client";
import React, { useState, useRef } from "react";
import {
  Col,
  Row,
  Typography,
  Space,
  Select,
  SelectProps,
  Dropdown,
} from "antd";
import Image from "next/image";
import { Icon } from "@iconify/react";
import { useWindowSize } from "react-use";

type LabelRender = SelectProps["labelRender"];

export default function HeaderForum() {
  const { width } = useWindowSize();
  const [items, setItems] = useState(["jack", "lucy"]);
  const [name, setName] = useState("");
  const options = [
    { label: "Vũng Tàu", value: "Vũng Tàu" },
    { label: "Đà Lạt", value: "Đà Lạt" },
    { label: "Nha Trang", value: "Nha Trang" },
    { label: "cyan", value: "cyan" },
  ];
  const labelRender: LabelRender = (props) => {
    const { label, value } = props;

    if (label) {
      return value;
    }
    return <span>Chọn địa điểm</span>;
  };
  const [selectedItems, setSelectedItems] = useState<string[]>([]);

  return (
    <>
      <Row>
        <Col sm={12} md={4}>
          <Row>
            <Typography.Title level={2}>Cộng đồng</Typography.Title>
          </Row>
          <Row>
            <Typography.Text>Show all communities</Typography.Text>
          </Row>
        </Col>
        <Col sm={12} md={8} className='max-sm:ml-5'>
          <Select
            labelRender={labelRender}
            defaultValue='1'
            style={{ width: "100%" }}
            options={options}
          />
        </Col>

        <Col
          sm={8}
          md={12}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          {width > 1300 && (
            <div className='container flex flex-column max-h-20'>
              <div className='w-[100%] flex flex-column '>
                <div className='w-[100%] flex flex-colum justify-end gap-3'>
                  <div
                    className='flex items-center px-3 py-3'
                    style={{ backgroundColor: "#e5e7eb", borderRadius: 8 }}
                  >
                    {" "}
                    <Icon icon='lets-icons:ring-light' width={40} />
                  </div>
                  <div
                    className='w-[28%] flex flex-colum justify-start '
                    style={{
                      border: "1px solid",
                      borderColor: "#A2A1A8",
                      borderRadius: 8,
                      overflow: "hidden",
                    }}
                  >
                    <div className='flex items-center relative right 15 max-h-[53px] '>
                      {" "}
                      <Image
                        className='mr-5 mt-5 pd-5'
                        width={44}
                        height={45}
                        src='https://scontent.fsgn2-3.fna.fbcdn.net/v/t1.6435-9/211330977_1225642591206832_5461207917626844039_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=5f2048&_nc_ohc=0CTrBlcCZ3MQ7kNvgGNqayM&_nc_ht=scontent.fsgn2-3.fna&oh=00_AYCmgftlzrs_jd3rofLTa5gfibjbwCuEm6a_GFnZq1rfjw&oe=6664FEC5'
                        alt='ss'
                        style={{ borderRadius: 15 }}
                      />
                    </div>
                    <div className=''>
                      <div>
                        {" "}
                        <Typography>
                          <Row>
                            {" "}
                            <Typography.Title level={3} className='mt-3'>
                              {" "}
                              Tk nguyen
                            </Typography.Title>
                          </Row>
                          <Row className='mt-[-13px]'> Admin</Row>
                        </Typography>
                      </div>
                    </div>
                    <div className='flex items-center'>
                      <Icon icon='icon-park-outline:down' width={35} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Col>
      </Row>
    </>
  );
}
