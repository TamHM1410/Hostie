import { message } from 'antd';
import { useCallback } from 'react';

const useNotification = () => {
    const [messageApi, contextHolder] = message.useMessage();

    const success = () => {
        messageApi.open({
            type: 'success',
            content: 'This is a success message',
        });
    };

    const errorMsg = (msg:any) => {
        messageApi.open({
            type: 'error',
            content: msg
        });
    };

    const warning = () => {
        messageApi.open({
            type: 'warning',
            content: 'This is a warning message',
        });
    };

    
    return { contextHolder, success, errorMsg, warning,messageApi};
};

export default useNotification;
